<?php

function node_scheduler_get_node_schedule($nid) {  
	if ($nid > 0) {
		$header = array(
											array('data'=>t('Type'), 'field'=>'schedule_type'),
											array('data'=>t('Name')),
											array('data'=>t('Field'), 'field'=>'field'),
											//array('data'=>t('Params')),
											array('data'=>t('Start Date'), 'field'=>'start_date'),
											array('data'=>t('End Date'), 'field'=>'end_date'),
											t('Actions'),null
										);
		$tablesort = tablesort_sql($header);
		
		//this needs to be done with hooks to other modules somehow?
		$sql = 'select * from {node_schedule}, {partial} where target_id = partial.pid and node_schedule.nid = %d' . $tablesort;
		$result = db_query($sql, $nid);
		
	return node_scheduler_db_result($result);
	}
}

function node_scheduler_db_result($result) {
	while ($entry = db_fetch_object($result)) {
		$schedule[$entry->sched_id] = $entry;
	}
	return $schedule;
}

function node_scheduler_remove_node_schedule($sched_id) {
	$result = db_query('delete from {node_schedule} where sched_id=%d', $sched_id);
}

function node_scheduler_clean_node_schedule($date) {
//	$result = db_query('delete from {node_schedule} where end_date < %d', $date);
}

function node_scheduler_remove_entire_node_schedule($nid) {
	$result = db_query('delete from {node_schedule} where nid=%d', $nid);
}

function node_scheduler_add_node_schedule($node_schedule) {
	$sched_id = db_next_id('node_schedule');
	$sql = 'insert into {node_schedule} (sched_id, schedule_type, nid, target_id, field, params, description, start_date, end_date)';
	$sql .= " values (%d, '%s', %d, %d, '%s', '%s', '%s', %d, %d)";
	db_query($sql,
					 $sched_id,
					 $node_schedule['schedule_type'],
					 $node_schedule['nid'],
					 $node_schedule['target_id'],
					 $node_schedule['field'],
					 $node_schedule['params'],
					 $node_schedule['description'],
					 $node_schedule['start_date'],
					 $node_schedule['end_date']
					 );
}